var CategoryRoiCalculator = {
  selector: '.js-roi-calculator',
  init: function() {
    if ($(this.selector).length > 0) {
      this.setup();
    }
  },
  setup: function() {
    this.$inputs = $(this.selector).find('.js-roi-calculator-input');
    this.$total = $(this.selector).find('#js-roi-calculator-total');
    this.$annual_total = $(this.selector).find('#js-roi-calculator-annual-total');
    this.$inputs.on('change', this.handleChange.bind(this));
    $(document).on('click', '.js-competitor-dropdown li', this.handleDropdownClick.bind(this));
    this.updateTotal();
    this.setDefaultCompetitors();
  },
  setDefaultCompetitors: function() {
    var self = this;
    $('.js-competitor-dropdown ul').each(function() {
      var $$ = $(this).children(':first');
      if ( $$.length > 0 ) {
        self.updateDropdownTitle($$);
      }
    });
  },
  handleDropdownClick: function(event) {
    this.updateDropdownTitle( $(event.currentTarget) );
  },
  updateDropdownTitle: function($selected) {
    var $title = $selected.parents('.dropdown').find('.dropdown-title');
    $title.html( $selected.html() );
  },
  handleChange: function() {
    this.updateTotal();
  },
  updateTotal: function() {
    var total = this.calculateTotal();
    this.$total.text('$' + total);
    this.$annual_total.text('$' + (total * 12));
  },
  calculateTotal: function() {
    var total = 0;
    this.$inputs.each(function() {
      var cost = Number.parseFloat($(this).val());
      total += Number.isNaN(cost) ? 0 : cost * 100;
    });
    return total / 100;
  }
};

$(CategoryRoiCalculator.init.bind(CategoryRoiCalculator));
